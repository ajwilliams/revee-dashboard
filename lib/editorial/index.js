var express = require('express');
var whiskers = require('whiskers');
var app = module.exports = express();

app.get('/editorial/*', function(req, res, next){
  req.session.context.editorialSelected = true; // this will change for every controller, for navigation purposes
  next();
});

app.get('/editorial/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'editorial/home.html';
  context.setTitle('Editorial');
  context.onMainPage = true;

  res.render('layout.html', context);
});

app.get('/editorial/toptenkeywords/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'editorial/top_ten_keywords.html';
  context.setTitle('Top Ten Key Words');
  context.topTenKeywordsSelected = true;

  res.render('layout.html', context);
});

app.get('/editorial/topperforming/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'editorial/top_performing.html';
  context.setTitle('Top Performing Pages');
  context.topPerformingSelected = true;

  res.render('layout.html', context);
});