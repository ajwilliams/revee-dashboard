var express = require('express');
var whiskers = require('whiskers');
var app = module.exports = express();

app.get('/sales/*', function(req, res, next){
  req.session.context.salesSelected = true; // this will change for every controller, for navigation purposes
  next();
});

app.get('/sales/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'sales/home.html';
  context.setTitle('Sales', 'Sales / Operations');
  context.onMainPage = true;

  res.render('layout.html', context);
});

app.get('/sales/topperforming/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'sales/top_performing.html';
  context.setTitle('Top Performing Pages');
  context.topPerformingSelected = true;

  res.render('layout.html', context);
});

app.get('/sales/widgets/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'sales/widgets.html';
  context.setTitle('Widgets', 'Widget Performance');
  context.widgetsSelected = true;

  res.render('layout.html', context);
});