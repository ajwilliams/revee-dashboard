var express = require('express');
var whiskers = require('whiskers');
var app = module.exports = express();

app.get('/marketing/*', function(req, res, next){
  req.session.context.marketingSelected = true; // this will change for every controller, for navigation purposes
  next();
});

app.get('/marketing/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'marketing/home.html';
  context.setTitle('Marketing');
  context.onMainPage = true;

  res.render('layout.html', context);
});

app.get('/marketing/rssfeeds/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'marketing/rss_feeds.html';
  context.setTitle('RSS Feeds');
  context.rssFeedsSelected = true;

  res.render('layout.html', context);
});

app.get('/marketing/createfeed/', function(req, res){
  var context = req.session.context;
  context.partials.mainContent = 'marketing/create_feed.html';
  context.setTitle('Create RSS Feed');
  context.createFeedSelected = true;

  res.render('layout.html', context);
});