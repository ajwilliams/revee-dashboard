var express = require('express');
var whiskers = require('whiskers');
var mysql = require('mysql');
var num = require('numeral');
var app = module.exports = express();
var reqNum = 0;
var STAT_OK = 'ok',
  STAT_ERR = 'error',
  STAT_WARN = 'warning';

app.get('/api/*', function(req, res, next){
  res.setHeader('Access-Control-Allow-Origin','*');
    //warnings = {reason:'ONEWORDDESCRIPTION',message:'Short message description',detailed_message:'Detailed message potentially with solutions'}
    //errors = {reason:'on of the following vals:'
      //not_modified | user_not_authenticated | unknown_data_source_id | access_denied | unsupported_query_operation | invalid_query | invalid_request | internal_error | not_supported | illegal_formatting_patterns | other
    //,message:'Short message description',detailed_message:'Detailed message potentially with solutions'}
    var tqx = req.query.tqx,
      params = tqx.split(';'),
      errors = [],
      version = '0.6',
      reqId,
      sig,
      respHandler = 'google.visualization.Query.setResponse',
      outFileName = '',
      out = 'json',
      returnVal = {
        'status' : STAT_OK // default to OK
      };
    for(var a = 0 ; a < params.length; a++) {
      var currParam = params[a].split(':')[0],
        currVal = params[a].split(':')[1];
      switch(currParam) {
        case 'version':
          version = currVal;
        break;
        case 'reqId':
          reqId = currVal;
        break;
        case 'sig':
          sig = currVal;
        break;
        case 'outFileName':
          outFileName = currVal;
        break;
        case 'out':
          out = currVal;
        break;
        case 'responseHandler':
          respHandler = currVal;
        break;
      }
    }
    if(typeof reqId !== 'undefined' && (reqId + '').length > 0) returnVal.reqId = reqId;

  /*
   * TODO: Make the JavaScript unlikely to execute when included with a <script src> tag. To do this, prefix your JSON response with )]}' followed by a newline. In your client strip the prefix from the response. For XmlHttpRequest this is only possible when the request originated from the same domain.
   */
  if(typeof req.headers['x-datasource-auth'] === 'undefined') errors.push({
      'reason' : 'access_denied',
      'message' : 'The request was not from the right domain',
      'detailed_message' : 'Try logging into the site again and reloading the page.'
    });
  req.session.apiObj = {
    'apiResp' : returnVal,
    'respHandler' : respHandler,
    'warnings' : [],
    'errors' : errors
  };
  next();
});


app.get('/api/vertBreakout', function(req, res, next){
  reqNum++;
  var tab = {
      'cols' : [
        {'id' : 'Date','label' : 'Date', 'type' : 'date'},
        {'id' : 'Homepage','label' : 'Homepage', 'type' : 'number'},
        {'id' : 'Entertainment','label' : 'Entertainment', 'type' : 'number'},
        {'id' : 'News','label' : 'News', 'type' : 'number'},
        {'id' : 'Sports','label' : 'Sports', 'type' : 'number'},
        {'id' : 'Health','label' : 'Health', 'type' : 'number'},
        {'id' : 'Finance','label' : 'Finance', 'type' : 'number'},
        {'id' : 'Movies','label' : 'Movies', 'type' : 'number'},
        {'id' : 'Celebrities','label' : 'Celebrities', 'type' : 'number'},
        {'id' : 'Cars','label' : 'Cars', 'type' : 'number'},
        {'id' : 'Politics','label' : 'Politics', 'type' : 'number'}
      ],
      'rows' : getVerticalsForDays(10, 30)
    };
  req.session.apiObj.apiResp.table = tab;
  next();
});

app.get('/api/*',function(req,res){
  var respObj = req.session.apiObj;
  delete req.session.apiObj;
  if(respObj.warnings.length > 0) {
    respObj.apiResp.status = STAT_WARN;
    respObj.apiResp.warnings = respObj.warnings;
  }
  if(respObj.errors.length > 0) {
    respObj.apiResp.status = STAT_ERR;
    respObj.apiResp.errors = respObj.errors;
    delete respObj.apiResp.table;
  }
  res.end(respObj.respHandler + '(' + JSON.stringify(respObj.apiResp) + ')');
});

function getVerticalsForDays(numVerticals, numDays) {
  var result = [];
  for(var a = 0; a < numDays; a++) result.push(getRow(numVerticals, a+1));

  return result;
}

function getRow(numVerticals, day) {
  var today = new Date(),
    d = new Date(today.getFullYear(),today.getMonth(),day),
      cs = [{
        'v' : 'Date(' + d.getFullYear()  + ',' + d.getMonth() + ',' + d.getDate() + ')',
        'f' : (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear()
      }];

  for(var a = 0; a < numVerticals; a++) {
    var val1 = Math.round(Math.random() * 1000000) + 50000;
    cs.push({
        'v' : val1,
        'f' : num(val1).format('0,0')
    });
  }

  return { 'c':cs };
}