var express = require('express');
var whiskers = require('whiskers');
var app = module.exports = express();

app.get('/login/', function (req, res, next) {
  var url = req.query.url || '',
    errorMessage = typeof req.session.errorMessage !== 'undefined' ? req.session.errorMessage : '';
  delete req.session.errorMessage;
  res.render('layout.html', {
    title : 'Login',
    pageHeader : 'Login',
    errorMessage : errorMessage,
    url : url,
    partials : {
      mainContent : 'login.html'
    },
  });
});

app.post('/login/', function (req, res, next) {
  var username = req.body.username,
    pass = req.body.password;
  if(req.session.user_display_name) res.redirect('/');
  else if (username === 'john' && pass === 'pass') { // TODO: hook in DB access
    var destination = req.body.url || '/';
    req.session.user_display_name = username;
    res.redirect(decodeURIComponent(destination));
  }
  else {
    req.session.errorMessage = 'Invalid Credentials';
    res.redirect('/login/');
  }
});

app.get('/logout/', function (req, res, next) {
  delete req.session.user_display_name;
  res.redirect('/login/');
});