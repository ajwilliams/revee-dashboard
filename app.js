/**
 * Session vars:
 * originalUrl : for tracking login forwarding
 * errorMessage : for providing errors in user friendly format (currently just in login)
 * user_display_name : for tracking when user is logged in
 */
var express = require('express');
//var combynExpress = require('combynexpress');
var whiskers = require('whiskers');
var bodyParser = require('body-parser');
var session = require('express-session'); // cookies are required for session, but cookie-parser is not
var app = express();

// App settings
app.engine('.html', whiskers.__express);
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('SITE_TITLE', ' | Revee');
app.set('IMAGE_DIR', __dirname + '/images/');
app.set('CSS_DIR', __dirname + '/css/');
app.set('JS_DIR', __dirname + '/js/');
app.use(express.static(app.get('IMAGE_DIR')));
app.use(express.static(app.get('CSS_DIR')));
app.use(express.static(app.get('JS_DIR')));
app.use(bodyParser());
app.use(session({secret: '1234567890QWERTY', resave: true, saveUninitialized: true})); // uses default options

// TODO: Find a way to set the views directory; 
// until then, all views will have to be under ./views/
// On that note, however, subdirs can be made inside ./views/
// and referenced in each servlet
// Modules

// NOTE (for combyne): Anything variables used in the template that are 
// not in the context, come up as undefined. MAKE SURE to
// safe check that all variables are sent!!

// NOTE (for whiskers): The variables are not encoded by default; make sure
// to encode potentially HTML-filled fields

/*
 * NOTE: Login process:
 * 1.) 401 (UnAuth) fires unauth page, which encodes original URL and appends it to the login button
 * 2.) User clicks login button which fires a GET to login servlet (which passes URL to template in hidden field)
 * 3.) User clicks to submit POST, is auth'd and, servlet decodes URL param and redirects to it
*/
var login = require('./lib/login');
app.use(login);

// authenticate every request
app.all('*', checkUser, function(req, res, next){
  req.session.context = {
    username: req.session.user_display_name,
    userLoggedIn : true,
    partials: {navBar : 'nav_bar.html'},
    setTitle: function(title, pageHeader){
      this.title = title + app.get('SITE_TITLE');
      this.pageHeader = typeof pageHeader !== 'undefined' ? pageHeader : title;
    },
    dbConfig : {
      host : 'localhost',
      post : 3306,
      user : 'root',
      password : '',
      database : 'recommendation'
    }
  };
  next();
});

var home = require('./lib/home');
var sales = require('./lib/sales');
var marketing = require('./lib/marketing');
var editorial = require('./lib/editorial');
var api = require('./lib/api');

// Instantiate modules
app.use(home);
app.use(sales);
app.use(marketing);
app.use(editorial);
app.use(api);

// Catch anything not matching as a 404
app.all('*', function(req, res){ res.send(404); }); // TODO: make custom 404 page

app.use(function(err, req, res, next){ // catch the error codes / custom rendering
  if(err instanceof Error){
    switch(err.message){
      case '401':
        var url = req.session.originalUrl || '';
        delete req.session.originalUrl;
        res.render('layout.html', {
          url : encodeURIComponent(url),
          title : 'Unauthorized Access',
          pageHeader : 'Unauthorized Access',
          partials : { mainContent : 'unauthorized.html' },
          userLoggedIn : false
        });
      break;
      default: // 500: server error
        res.send('<div>SERVER ERROR:</div><div>' + err + '</div>'); // TODO make custom 5xx page
      break;
    }
  }
});

// Start app
app.listen(app.get('port'), 
  function(){
    console.log('Express server listening on port: ' + app.get('port'));
    console.log('Image directory is set as: ' + app.get('IMAGE_DIR'));
    console.log('CSS directory is set as: ' + app.get('CSS_DIR'));
    console.log('JS directory is set as: ' + app.get('JS_DIR'));
  }
);

function checkUser(req, res, next) {
  if (!req.session.user_display_name) {
    req.session.originalUrl = encodeURIComponent(req.originalUrl);
    next(new Error(401));
  }
  else next();
}

module.exports = app;
