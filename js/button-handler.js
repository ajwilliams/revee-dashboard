jQuery(document).ready(function($){
  $('.button').not('.selected').on('click', function(e){
    var param = $(this).data('param'),
      val = $(this).data('val'),
      currLoc = (window.location.search || window.location.pathname),
      newLoc = window.location.pathname + '?';
    /*
    if(currLoc.indexOf('?') > -1) {// there were already params
      currLoc = currLoc.substring(currLoc.indexOf('?') + 1); // get the '?' out
      var currParams = currLoc.split('&'), // separate params
        foundParam = false;
      newLoc += '?'; // begin new location
      for(var a = 0 ; a < currParams.length; a++){ // for every param
        var tmpParamParts = currParams[a].split('='); // get param name and val separated
        if(tmpParamParts[0] == param) { 
          currParams[a] = param + '=' + val;
          foundParam = true;
        }// if the param was already in query, update the value
        newLoc += currParams[a]; // add param to the new location
        if(a < currParams.length - 1) newLoc += '&'; // if there are more params, separate with ampersand
      }
      if(!foundParam) newLoc += '&' + param + '=' + val; // the param wasn't in the string before, so we append it
    }
    else newLoc += '?' + param + '=' + val; // there were no params
    */
   
   var btnVals = $('.button.selected').not('[data-param="' + param + '"]'),
     textVals = $('input[type="text"]');
   if($(this).data('type') !== 'filter') newLoc += param + '=' + val;
   btnVals.each(function(i,e){
     newLoc += '&' + $(e).data('param') + '=' + $(e).data('val');
   });
   textVals.each(function(i,e){
     newLoc += '&' + $(e).data('param') + '=' + $(e).val();
   });

   document.location.href = newLoc;
  });
});