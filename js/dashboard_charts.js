google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
google.setOnLoadCallback(drawChart2);
window.addEventListener('resize', function(){
  drawChart();
  drawChart2();
});
var availColors = {
  'revee-red':'#f00',
  'bright-blue':'#4bafd8',
  'green':'#46a200',
  'orange':'#f93',
  'purple':'#9f00e7',
  'yellow':'#ffd500',
  'blue':'#00f',
  'light-grey':'#999',
  'mid-grey':'#666',
  'dark-grey':'#333'
};
var comparisonData = [
  ['Source', 'Traffic'],
  ['Direct', 150],
  ['Indirect', 350],
  ['House', 80]
];
function drawChart() {
  var chart = new google.visualization.PieChart(document.getElementById('cht-direct-compare')),
    data = google.visualization.arrayToDataTable(comparisonData),
    options = {
      colors: [availColors['light-grey'], availColors['dark-grey'], availColors['mid-grey']],
      pieHole: 0.7,
      pieSliceText: 'none'
    };
  chart.draw(data, options);
}
function drawChart2() {
  var allColors = [];
  for(var color in availColors) allColors.push(availColors[color]);
  var chart = new google.visualization.LineChart(document.getElementById('cht-vert-breakout')),
    options = {
      colors: allColors,
      curveType: 'function',
      focusTarget: 'category',
      legend: { position: 'top' },
      pointSize: 5,
      animation: {
        duration: 1500,
        easing: 'inAndOut'
      },
      vAxis: {
        gridlines: { count: 4 }
      }
    };
  //var reg = setInterval(function(){
    var query = new google.visualization.Query('/api/vertBreakout/');
    query.send(function(resp){ updateChart(resp, chart, options); });
  //},1000 );
}

function updateChart(resp, chart, opt){
  if (resp.isError()) {
    alert('Error in query: ' + resp.getMessage() + ' ' + resp.getDetailedMessage());
    return;
  }

  var data = resp.getDataTable();
  chart.draw(data, opt);
}